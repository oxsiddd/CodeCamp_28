'use strict'
const fs = require('fs')


let head = new Promise(function (resolve, reject) {
    fs.readFile('head.txt', 'utf8', function(err, data){
        if(err)
            reject(err)
        else
            resolve(data)
    })
})
let body = new Promise(function (resolve, reject) {
    fs.readFile('body.txt', 'utf8', function(err, data){
        if(err)
            reject(err)
        else
            resolve(data)
    })
})
let leg = new Promise(function (resolve, reject) {
    fs.readFile('leg.txt', 'utf8', function(err, data){
        if(err)
            reject(err)
        else
            resolve(data)
    })
})
let feet = new Promise(function (resolve, reject) {
    fs.readFile('feet.txt', 'utf8', function(err, data){
        if(err)
            reject(err)
        else
            resolve(data)
    })
})
function makeRobot(newResult) {
    return new Promise(function(resolve, reject){
        fs.writeFile('robot2.txt', newResult, 'utf8', function(err, data){
            if(err)
                reject(err)
            else
                resolve("Success!")
        })
    })
}

async function robotComplete() {
    try {
        let result = await Promise.all([head, body, leg, feet])
        let newResult = result.reduce((result, index) => {
            return result + `\n${index}`
        })
        await makeRobot(newResult)
        console.log(newResult)
    } catch (error) {
        console.error(error);
    }
}  
robotComplete();

// Promise.all([head, body, leg, feet])
// .then(function(result){

//     const newResult = result.reduce((result, index) => {
//         return result + `\n${index}`
//     })

//     fs.writeFile('robot2.txt', newResult, 'utf8', function(err, data){
//         if(err)
//             console.error("There's an error", error)
//     })
//     console.log(newResult)
// })
// .catch(function(error){
//     console.error("There's an error", error)
// })
