'use strict'
const fs = require('fs')

let newData = ''

function readHead() {
    return new Promise(function(resolve, reject){
        fs.readFile('head.txt', 'utf8', function(err, data){
            if(err)
                reject(err)
            else
                newData += `${data}\n`
                resolve(newData)
        })
    })
}

function readBody() {
    return new Promise(function(resolve, reject){
        fs.readFile('body.txt', 'utf8', function(err, data){
            if(err)
                reject(err)
            else
                newData += `${data}\n`
                resolve(newData)
        })
    })
}

function readLeg() {
    return new Promise(function(resolve, reject){
        fs.readFile('leg.txt', 'utf8', function(err, data){
            if(err)
                reject(err)
            else
                newData += `${data}\n`
                resolve(newData)
        })
    })
}

function readFeet() {
    return new Promise(function(resolve, reject){
        fs.readFile('feet.txt', 'utf8', function(err, data){
            if(err)
                reject(err)
            else
                newData += `${data}\n`
                resolve(newData)
        })
    })
}

function makeRobot(newData) {
    return new Promise(function(resolve, reject){
        fs.writeFile('robot.txt', newData, 'utf8', function(err, data){
            if(err)
                reject(err)
            else
                resolve("Success!")
        })
    })
}

async function robotComplete() {
    try {
        await readHead()
        await readBody()
        await readLeg()
        await readFeet()
        await makeRobot(newData)
        console.log(newData);
    } catch (error) {
        console.error(error);
    }
}  
robotComplete();