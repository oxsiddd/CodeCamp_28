const Koa = require('koa')
const session = require('koa-session')

const app = new Koa()

const sessionConfig = {
    key: 'sess',
    maxAge: 1000 * 60 * 60,
    httpOnly: true
}

app.keys = ['supersecret']
app.use(session(sessionConfig, app))
app.use(handler)
app.listen(8000)

function handler (ctx) {
    if (ctx.path === '/favicon.ico') 
    return 
    let n = ctx.session.views || 0 
    ctx.session.views = n++
    ctx.body = `${n} views`
}
