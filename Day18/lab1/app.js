const bcrypt = require('bcrypt')
const password = 'asdfg'

async function hashed() {
    const hashedPassword = await bcrypt.hash(password, 10)
    // console.log(hashedPassword)
}
hashed()

async function compare() {
    const hashedPassword = '$2b$10$irXfF8R8SRGIUMnP/.cjbu/99DQiegMBrgTHhxBChYMwFIcdr.BEq'
    const same = await bcrypt.compare('password', hashedPassword)
    if (!same) {
        console.log('wrong password')
    } else {
        console.log('password correct')
    }
}
compare()