'use strict'

fetch('./homework2_1.json').then(response => {
    return response.json();
}).then(employees => {
    addYearSalary(employees[0])
    addNextSalary(employees[0])
    addAdditionalFields(employees)
    console.log(employees)

    let showEmployees = employees
    showEmployees[0].salary = 100
    let headTable = [] //Array Row head <th>
    let valueTable = [] //Array Row value <td>
        
    
    for(let people of showEmployees){
        let obj = {}
        //console.log(people)
        for(let data in people){

            //console.log(data)
            if(headTable.indexOf(data) < 0){
                headTable.push(data)
            }
            obj[data] = people[data]
            //console.log(people[data])
        }
            
        valueTable.push(obj)

    }

    $('#dataTable').append('<tr>')
    for(let x in headTable){
        $('#dataTable').append('<th>'+ headTable[x] + '</th>')
    //console.log(headTable)
    }
    $('#dataTable').append('</tr>')

    for (let x in valueTable){
        $('#dataTable').append('<tr>')
        //console.log(x)
        for (let y in valueTable[x]){
            if(y !== "nextSalary"){
                $('#dataTable').append('<td>'+valueTable[x][y]+'</td>')
            
            }else if(y == "nextSalary"){
                $('#dataTable').append('<td><ol>'+
                '<li>'+valueTable[x][y][0]+'</li>'+
                '<li>'+valueTable[x][y][1]+'</li>'+
                '<li>'+valueTable[x][y][2]+'</li>'+
                '</ol></td>')
                //console.log(valueTable[x][y])
            }
        }
        $('#dataTable').append('</tr>')
        //console.log(valueTable)
    }

    let newEmployees = []
    newEmployees = employees
    newEmployees[0].salary = 40000
    console.log(newEmployees)

    let headTable2 = [] //Array Row head <th>
    let valueTable2 = [] //Array Row value <td>
        
    
    for(let people of newEmployees){
        let obj = {}
        //console.log(people)
        for(let data in people){
            //console.log(data)
            if(data !== "yearSalary" && data !== "nextSalary"){
                if(headTable2.indexOf(data) < 0){
                    headTable2.push(data)
                }
                obj[data] = people[data]
                //console.log(people[data])
            }
        }
        
        valueTable2.push(obj)

    }
    // console.log(valueTable)
    // console.log(headTable)
    $('#newDataTable').append('<tr>')
    for(let x in headTable2){
        $('#newDataTable').append('<th>'+ headTable2[x] + '</th>')
        //console.log(headTable)
    }
    $('#newDataTable').append('</tr>')
    
    for (let x in valueTable2){
        $('#newDataTable').append('<tr>')
        for (let y in valueTable2[x]){
            $('#newDataTable').append('<td>'+valueTable2[x][y]+'</td>')
            //console.log(valueTable[x][y])
        }
        $('#newDataTable').append('</tr>')
        //console.log(valueTable)
    }



}).catch(error => {
    console.log('Error:', error)
})

function addYearSalary(row) {
    row.yearSalary = row.salary * 12
    return console.log(row)
}

function addNextSalary(row) {
    
   let nextSalary1 = parseInt(row.salary)
   let nextSalary2 = (nextSalary1 * 0.1) + nextSalary1
   let nextSalary3 = (nextSalary2 * 0.1) + nextSalary2

    
    row.nextSalary = [nextSalary1, nextSalary2, nextSalary3]
    console.log(nextSalary1)
    console.log(nextSalary2)
    console.log(nextSalary3)
}

function addAdditionalFields(all){
    for(let data in all){
        if(data != 0){
            addYearSalary(all[data])
            addNextSalary(all[data]) 
        }
    }
    return all

}


            
