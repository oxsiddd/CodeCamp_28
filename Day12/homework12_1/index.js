const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()
const pool = mysql.createPool({
    connectionLimit: 2,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'db_codecamp'
})

router.get('/', async(ctx, next) => {
    let [rowsEmp, fieldsEmp] = await pool.query('select firstname, lastname, age from employees')
    let [rowsBook, fieldsBook] = await pool.query('select isbn, title, price from books')
    
    const empKey = Object.keys(rowsEmp[0])
    const bookKey = Object.keys(rowsBook[0])

    const data = {
        title: 'Homework12_1',
        employee: 'Employees',
        book: 'Books',
        eKey: empKey,
        bKey: bookKey,
        eData: rowsEmp,
        bData: rowsBook
    }
    await ctx.render('home', data)
})

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)