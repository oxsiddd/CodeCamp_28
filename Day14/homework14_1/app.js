const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const db = require('./libs/connect')

const app = new Koa()
const router = new Router()

router.get('/', async(ctx, next) => {
    const sumEnrollSql = 'select sum(price) as total_enrolls from courses inner join enrolls on enrolls.course_id = courses.id'
    const studentSql = 'select student_id as id,s.name as student_name, enroll from ( select student_id, (select price from courses where courses.id = enrolls.course_id) as enroll from enrolls) as t inner join students as s on s.id = student_id order by student_id'

    const [rowsSumEnroll, fieldsSumEnroll] = await db.query(sumEnrollSql)
    const [rowsStudent, fieldsStudent] = await db.query(studentSql)
    // console.log(rows)
    const keySumEnroll = await Object.keys(rowsSumEnroll[0])
    const keyStudent = await Object.keys(rowsStudent[0])
    console.log(keySumEnroll)
    console.log(keyStudent)
    const data = {
        title: 'Homework14_1',
        name_table1: 'All courses to enrolls',
        name_table2: 'Students enroll course',
        key_sum: keySumEnroll[0],
        data_sum: rowsSumEnroll,
        key_student: keyStudent,
        data_student: rowsStudent
    }
    await ctx.render('home', data)
    await next()
})

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

app.use(router.routes())
app.use(router.allowedMethods())
app.use(serve(path.join(__dirname, 'public')))
app.listen(3000)