import React from 'react'

const ItemTodo = ({ todoItem }) => (
    <div>
        <p>Todo: {todoItem.todo}</p>
        <hr />
    </div>
)
export default ItemTodo