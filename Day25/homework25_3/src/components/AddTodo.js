import React, { Component } from 'react'

class AddTodo extends Component {
    
    state = {
        todo: ''
    }

    handlerChange = (e) => {
        this.setState({ [e.target.name]: e.target.value })
    }

    handlerSubmit = (e) => {
        e.preventDefault()
        this.props.onSubmit(this.state)
        this.setState({ todo: '' })
    }

    render () {
        const { todo } = this.state
        return (
            <div>
                <form onSubmit={this.handlerSubmit}>
                    <input name="todo" value={todo} onChange={this.handlerChange} />
                    <button>Add Todo</button>
                </form>
            </div>
        )
    }
}

export default AddTodo