import React from 'react'
import ItemTodo from '../components/ItemTodo'

const ListTodo = ({ todos }) => (
    <div>
        {todos.map(t => (
            <ItemTodo key={t.id} todoItem={t} />
        ))}
    </div>
)

export default ListTodo