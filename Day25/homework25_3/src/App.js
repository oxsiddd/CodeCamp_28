import React, { Component } from 'react'
import AddTodo from './components/AddTodo'
import ListTodo from './components/ListTodo'

class App extends Component {
  state = {
    nextId: 3,
    todos: [
      {
        id: 1,
        todo: 'Happy',
      },
      {
        id: 2,
        todo: 'Unhappy',
      }
    ]
  }

  onSubmit = (todoItem) => {
    this.setState({
      nextId: this.state.nextId + 1,
      todos: [
        ...this.state.todos,
        { id: this.state.nextId, todo: todoItem.todo },
      ],
    })  
  }

  render() {
    return (
      <div>
        <h1>To do ..</h1>
        <AddTodo onSubmit={this.onSubmit} />
        <ListTodo todos={this.state.todos} />
      </div>
    );
  }
}

export default App