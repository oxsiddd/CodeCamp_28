let birthday = 0
for(let roundBirthDay = 0; roundBirthDay < 31; roundBirthDay++){
    birthday += 1        
    $('#birthday').append('<option>'+ birthday + '</option>')
            
}

let month = 0
for(let roundMonth = 0; roundMonth < 12; roundMonth++){
    month += 1        
    $('#month').append('<option>'+ month + '</option>')
            
}

let year = 1979
for(let roundYear = 0; roundYear < 26; roundYear++){
    year += 1
    $('#year').append('<option>'+ year + '</option>')
}

function checkform() {
    let registerForm = document.getElementById('registerForm')
    if(registerForm.password.value != registerForm.repassword.value){
        alert("Password ไม่เหมือนกัน กรุณากรอกใหม่")
        registerForm.repassword.focus()
        return false
    }else if(registerForm.email.value != registerForm.reemail.value){
        alert("E-mail ไม่เหมือนกัน กรุณากรอกใหม่")
        registerForm.reemail.focus()
        return false
    }else{
        return true
    }
}