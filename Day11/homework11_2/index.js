const Koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const serve = require('koa-static')
const mysql = require('mysql2/promise')
const fs = require('fs')

const app = new Koa()
const router = new Router()
const pool = mysql.createPool({
    connectionLimit: 2,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'db_codecamp'
})

router.get('/', async (ctx, next) => {
    ctx.body = 'Homepage'
    await next()
})

router.get('/from_database', async (ctx, next) => {
    ctx.namePath = 'from_database'
    await next()
})

router.get('/from_file', async (ctx, next) => {
    ctx.namePath = 'from_file'
    await next()
})

async function changeFormat(ctx, next) {
    let[rows, fields] = await pool.query('select id, firstname, lastname, salary, role from users')
    console.log(rows[0])
    
    let dataFile = await new Promise(function (resolve, reject) {
        fs.readFile('../../28Apr/homework2_1.json', function(err, data){
            if(err)
                reject(err)
            else
                resolve(data)
        })
    })
    let dataConvert = JSON.parse(dataFile)
    console.log(dataConvert[0])

    let obj1 = {
        data: [
            rows[0]
        ],
        additionalData: {
            userId: 1,
            dateTime: now()
        }
    }
    let obj2 = {
        data: [
            dataConvert[0]
        ],
        additionalData: {
            userId: 1,
            dateTime: now()
        }
    }
    if(ctx.namePath === 'from_database')
        ctx.body = JSON.stringify(obj1, undefined, 2)
    if(ctx.namePath === 'from_file')
        ctx.body = JSON.stringify(obj2, undefined, 2)
    else
    await next()
}

function now(){
    const year = new Date()
    nowYear = year.getFullYear()
    const month = new Date()
    nowMonth = month.getMonth() + 1
    const date = new Date()
    nowDate = date.getDate()
    const hour = new Date()
    nowHour = hour.getHours()
    const minute = new Date()
    nowMinute = hour.getMinutes()
    const second = new Date()
    nowSecond = hour.getSeconds()
    const nowTime = nowYear+'-'+nowMonth+'-'+nowDate+' '+nowHour+':'+nowMinute+':'+nowSecond

    return nowTime  
}



app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(changeFormat)
app.use(router.allowedMethods())
app.listen(3000)