const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const mysql = require('mysql2/promise')

const app = new Koa()
const router = new Router()
const pool = mysql.createPool({
    connectionLimit: 2,
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'db_codecamp'
})

router.get('/', async(ctx, next) => {
    let [rows, fields] = await pool.query('select id, firstname, lastname, salary, role from users')
    console.log(rows)
    const key = Object.keys(rows[0])
    const data = {
        title: 'Homework11_1',
        user: 'Users',
        key: key,
        data: rows
    }
    await ctx.render('home', data)
})

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)