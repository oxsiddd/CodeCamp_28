const fs = require('fs')

let ReadFileData = async function() {
    try {
        let dataFile = await new Promise(function (resolve, reject) {
            fs.readFile(__dirname + '/homework2_1.json', function(err, data){
                if(err)
                    reject(err)
                else
                    resolve(data)
            })
        })
        let dataConvert = JSON.parse(dataFile)
        console.log(dataConvert[0])
        return dataConvert[0]
    } catch (error) {
        console.error(error)
    }   
}

module.exports.ReadFileData = ReadFileData()