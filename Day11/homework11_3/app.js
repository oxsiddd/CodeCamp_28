const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const qDB = require('./models/from_database')
const rFD = require('./models/from_file')

const app = new Koa()
const router = new Router()

router.get('/', async(ctx, next) => {
    ctx.body = 'Homepage'
    await next()
})

router.get('/from_database', async(ctx, next) => {
    ctx.namePath = 'from_database'
    await next()
})

router.get('/from_file', async(ctx, next) => {
    ctx.namePath = 'from_file'
    await next()
})

async function changeFormat(ctx, next) {
    try {
        let obj1 = {
            data: [
                await qDB.QueryDB
            ],
            additionalData: {
                userId: 1,
                dateTime: now()
            }
        }
        let obj2 = {
            data: [
                await rFD.ReadFileData
            ],
            additionalData: {
                userId: 1,
                dateTime: now()
            }
        }
        if(ctx.namePath === 'from_database')
            ctx.body = JSON.stringify(obj1, undefined, 2)
        if(ctx.namePath === 'from_file')
            ctx.body = JSON.stringify(obj2, undefined, 2)
        else
        await next()
    } catch (error) {
        console.error(error)
    }
}


function now(){
    const year = new Date()
    nowYear = year.getFullYear()
    const month = new Date()
    nowMonth = month.getMonth() + 1
    const date = new Date()
    nowDate = date.getDate()
    const hour = new Date()
    nowHour = hour.getHours()
    const minute = new Date()
    nowMinute = hour.getMinutes()
    const second = new Date()
    nowSecond = hour.getSeconds()
    const nowTime = nowYear+'-'+nowMonth+'-'+nowDate+' '+nowHour+':'+nowMinute+':'+nowSecond

    return nowTime  
}

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(changeFormat)
app.use(router.allowedMethods())
app.listen(3000)

