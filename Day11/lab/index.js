const Koa = require('koa')
const Router = require('koa-router')
const path = require('path')
const serve = require('koa-static')
const app = new Koa()
const router = new Router()
app.use(serve(path.join(__dirname, 'public')))

router.get('/', async (ctx, next) => {
    ctx.myVariable = 'allow'; // assume variable
    await next();
})
router.get('/forbidden', async (ctx, next) => {
    ctx.myVariable = 'forbidden'; // assume variable
    await next();
})
async function myMiddleware (ctx, next) {
    if (ctx.myVariable === 'allow')
        ctx.body = "Hello World";
    if (ctx.myVariable === 'forbidden')
        ctx.body = "Forbidden";
    else
        await next();
}
 app.use(router.routes())
 app.use(myMiddleware)
 app.listen(3000)

