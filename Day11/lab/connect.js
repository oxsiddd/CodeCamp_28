const mysql = require('mysql2/promise')
const pool = mysql.createPool({
    connectionLimit : 10,
    host : 'localhost',
    user : 'root',
    password : 'omp123',
    database : 'codecamp'
})

async function myQuery() {
    let [rows, fields] = await pool.query('SELECT * FROM user')
    console.log('The solution is: ', rows, fields)
}
myQuery()