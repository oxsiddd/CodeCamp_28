import React, { Component } from 'react';
import './App.css';

async function fetchData(){
  let data = (await fetch('https://randomuser.me/api/')).json()
  // console.log(data.json())
  return data
}
class App extends Component {
  state = {
    name: '',
    email: '',
    gender: '',
    imgSrc: ''
  }

  handlerGen = async () => {
    let item = await fetchData()
    this.setState({
      email: item.results[0].email,
      gender: item.results[0].gender,
      imgSrc: item.results[0].picture.large,
      name: `${item.results[0].name.title} ${item.results[0].name.first} ${item.results[0].name.last}`
    })
  }
  
  
  render() {
    return (
      <div className="App">
        
        <img src={this.state.imgSrc} />
        <p>email: {this.state.email}</p>
        <p>gender: {this.state.gender}</p>
        <p>name: {this.state.name}</p>
        <button onClick={this.handlerGen}>Generate User</button>
      </div>
    );
  }
}

export default App;
