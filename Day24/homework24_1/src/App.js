import React, { Component } from 'react';
import './App.css';
import PictureCard from './components/PictureCard'

class App extends Component {
  data = [
    {id: 1, imgSrc: "", createBy: "John", date: "", likeCount: 12, commentCount: 22 },
    {id: 2, imgSrc: "", createBy: "Jane", date: "", likeCount: 2, commentCount: 32 },
    {id: 3, imgSrc: "", createBy: "Tom", date: "", likeCount: 22, commentCount: 12 },
    {id: 4, imgSrc: "", createBy: "Tim", date: "", likeCount: 42, commentCount: 23 },
    {id: 5, imgSrc: "", createBy: "Ward", date: "", likeCount: 102, commentCount: 2 }
  ]
  newDate = new Date()
  render() {
    return (
      <div className="App">
        {} 
      </div>
    );
  }
}

export default App;
