class Employee {
    constructor(firstname, lastname, salary) {
        this. _salary = salary // simulate private variable

        this.firstname = firstname
        this.lastname = lastname
        //this.getSalaryPrivate = () => _salary
    }
    setSalary(newSalary) { // simulate public method
        // return newSalary ถ้ามีเงินเดือนใหม่มีค่ามากกว่า this._salary
        if(newSalary > this._salary){

            return newSalary
        }else{
            return false
        }
        
        // return false ถ้าเงินเดือนใหม่มีค่าน้อยกว่าเท่ากับ this._salary   
    }
    getSalary () {  // simulate public method
        return this._salary
    }
    gossip (employee, talk) {
        console.log(`Hey ${employee.firstname},${talk}`)
    }

    work(employee) {
        // leave blank for child class to be overidden
    }
    leaveForVacation(year, month, day) {

    }
}
exports.Employee = Employee