const { Employee } = require('./employee')
const { CEO } = require('./ceo')
const { Programmer } = require('./programmer')

let dang = new Employee('Dang','Red', 10000);
let ceo = new CEO('Somchai','Sudlor', 30000);
let code = new Programmer('John','Doe',20000)

/////////////// EMP
console.log(dang.firstname); // Dang
console.log(dang.lastname); // Red
dang.hello(); // Hello Dang!
console.log(dang.getSalary())

////////////////////////CEO
console.log(ceo.firstname); // Dang
console.log(ceo.lastname); // Red
ceo.hello(); // Hello Dang!
console.log(ceo.getSalary())

////////////////////////PGM
console.log(code.firstname); // Dang
console.log(code.lastname); // Red
code.hello(); // Hello Dang!
console.log(code.getSalary())