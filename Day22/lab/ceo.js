const { Employee } = require('./employee')
class CEO extends Employee {
    constructor(firstname, lastname, salary) {
        super(firstname, lastname, salary); //SUPER!!!!!
    }
    getSalary(){ // simulate public method
        return super.getSalary()*2;
    };
    hello() { // simulate public method
        console.log("Hi, nice to meet you. "+this.firstname+"!");
    }
}
exports.CEO = CEO