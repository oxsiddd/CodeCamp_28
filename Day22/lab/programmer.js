const { Employee } = require('./employee')
class Programmer extends Employee {
    constructor(firstname, lastname, salary){
        super(firstname, lastname, salary)
    }
    getSalary(){
        return super.getSalary()*1.5
    }
    hello(){
        console.log("!@#$%^&"+this.firstname)
    }
}
exports.Programmer = Programmer