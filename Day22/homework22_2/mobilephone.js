class MobilePhone {
    constructor(brand, model, price) {
        this.brand = brand
        this.model = model
        this.price = price
    }
    phoneCall () {
        return console.log(`${this.brand}/${this.model} Phone Call:passed!`)
    }
    sms () {
        return console.log(`${this.brand}/${this.model} SMS:passed!`)
    }
    internetSurfing () {
        return console.log(`${this.brand}/${this.model} Surfing:passed!`)
    }
}
exports.MobilePhone = MobilePhone