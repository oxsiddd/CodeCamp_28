const { MobilePhone } = require('./mobilephone')
class S8 extends MobilePhone {
    constructor(brand, model, price) {
        super(brand, model, price)
    }
    allFunction () {
        super.phoneCall()
        super.sms()
        super.internetSurfing()
        this.useGearVR()
        this.tranfromToPC()
        this.googlePlay()
    }
    useGearVR () {
        return console.log(`${this.brand}/${this.model} :can use Gear VR`)
    }
    tranfromToPC () {
        return console.log(`${this.brand}/${this.model} :can tranfrom to PC`)
    }
    googlePlay () {
        return console.log(`${this.brand}/${this.model} :can use Googleplay`)
    }
}
exports.S8 = S8