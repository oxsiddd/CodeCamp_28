const { MobilePhone } = require('./mobilephone')
class Note8 extends MobilePhone {
    constructor(brand, model, price) {
        super(brand, model, price)
    }
    allFunction () {
        super.phoneCall()
        super.sms()
        super.internetSurfing()
        this.useGearVR()
        this.tranfromToPC()
        this.usePen()
        this.googlePlay()
    }
    useGearVR () {
        return console.log(`${this.brand}/${this.model} :can use Gear VR`)
    }
    tranfromToPC () {
        return console.log(`${this.brand}/${this.model} :can tranfrom to PC`)
    }
    usePen () {
        return console.log(`${this.brand}/${this.model} :can use SPen`)
    }
    googlePlay () {
        return console.log(`${this.brand}/${this.model} :can use Googleplay`)
    }
}
exports.Note8 = Note8