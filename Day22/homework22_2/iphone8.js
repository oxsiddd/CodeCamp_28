const { MobilePhone } = require('./mobilephone')
class Iphone8 extends MobilePhone {
    constructor(brand, model, price) {
        super(brand, model, price)
    }
    allFunction () {
        super.phoneCall()
        super.sms()
        super.internetSurfing()
        this.touchID()
        this.appStore()
    }
    touchID () {
        return console.log(`${this.brand}/${this.model} :can use Touch ID`)
    }
    appStore () {
        return console.log(`${this.brand}/${this.model} :can use App Store`)
    }
}
exports.Iphone8 = Iphone8