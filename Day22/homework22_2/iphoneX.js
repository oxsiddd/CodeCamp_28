const { MobilePhone } = require('./mobilephone')
class IphoneX extends MobilePhone {
    constructor(brand, model, price) {
        super(brand, model, price)
    }
    allFunction () {
        super.phoneCall()
        super.sms()
        super.internetSurfing()
        this.faceID()
        this.appStore()
    }
    faceID () {
        return console.log(`${this.brand}/${this.model} :can use Face ID`)
    }
    appStore () {
        return console.log(`${this.brand}/${this.model} :can use App Store`)
    }
}
exports.IphoneX = IphoneX