'use strict'

// Async Serial
console.log("start");
function async1() {
  console.log("no.2");
  setTimeout(() => {
    console.log("no.3");
    async2();
  },3000);
}
function async2() {
  console.log("no.4");
  setTimeout(() => {
    console.log("no.5");
  },3000);
} 
console.log("no.1"); async1();

// callback

// let callbackValue = 'callbackValueOld';
// function tryHello5() {
//   let returnValue = 'returnValue';
//   let callbackValue = 'callbackValueNew';
//     setTimeout((err, value) => {
//         callbackValue = value;
//         console.log(callbackValue); // print callbackValueNew after 3 sec
//     }, 3000, null, callbackValue);
//     return returnValue; 
// }
// console.log(tryHello5()); // print returnValue
// console.log(callbackValue); // print callbackValueOld