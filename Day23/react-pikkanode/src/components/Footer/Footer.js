import React, { Component } from 'react'
// import { Container, Row, Col } from 'reactstrap'
import './Footer.css'
import heart from '../../assets/img/like.svg'

class Footer extends Component {
    render () {
        return (
            <blockquote className="blockquote text-center">
                <h6 className="mb-0">made with <img src={heart} width="14" /> by</h6>
                <footer className="blockquote-footer">CodeCamp <cite title="Source Title" className="text-info">#28</cite></footer>
            </blockquote>
            
        )
    }
}

export default Footer