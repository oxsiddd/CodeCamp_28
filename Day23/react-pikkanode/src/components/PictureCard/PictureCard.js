import React, { Component } from 'react'
import './PictureCard.css'
import { 
    Card,
    CardColumns, 
    CardImg, 
    CardText, 
    CardBody,
    CardTitle, 
    CardSubtitle } from 'reactstrap'
import like from '../../assets/img/like.svg'
import comment from '../../assets/img/chat.svg'

class PictureCard extends Component {
    render () {
        return (
            <div className="mt-5 "> 
                <Card className="shadow-sm bg-white rounded">
                    <CardImg top width="100%" src={this.props.pic} />
                    <CardBody>
                        <CardTitle>{this.props.name}</CardTitle>
                        
                        <a><img src={like} width="24" /> {this.props.like}</a>
                        <a className="float-right"><img src={comment} width="24" /> {this.props.comment}</a>
                        <hr />
                        <CardSubtitle className="text-right">{this.props.date}</CardSubtitle>
                    </CardBody>
                </Card>
            </div>
        )
    }
}

export default PictureCard