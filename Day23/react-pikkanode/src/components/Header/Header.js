import React, { Component } from 'react'
import './Header.css';
import {
    Collapse,
    Navbar,
    NavbarToggler,
    NavbarBrand,
    Nav,
    NavItem,
    Button } from 'reactstrap'
import { Link } from 'react-router-dom'
import logo from '../../assets/img/pikkanode.png'

class Header extends Component {
    constructor(props) {
        super(props);

        this.toggle = this.toggle.bind(this);
        this.state = {
        isOpen: false
        };
    }
    toggle() {
        this.setState({
        isOpen: !this.state.isOpen
        });
    }

    render () {
        return (
            <div>
                <Navbar color="light" expand="md" className="shadow-sm bg-white rounded">
                <NavbarBrand href="/"><img src={logo} height="54" /></NavbarBrand>
                <NavbarToggler onClick={this.toggle} />
                <Collapse isOpen={this.state.isOpen} navbar>
                    <Nav className="mr-auto sm-padding" navbar>
                    {/* <NavItem className="nav-link">
                        <Link to="/">Home</Link>
                    </NavItem>
                     */}
                    </Nav>
                    <Nav className="ml-auto sm-padding" navbar>
                    <NavItem className="nav-link none none-hover">
                        <Link to="/signin"><Button className="btn-quote">sign in</Button></Link>
                    </NavItem>
                    <NavItem className="nav-link none none-hover">
                        <Link to="/signup"><Button className="btn-quote">sign up</Button></Link>
                    </NavItem>
                    </Nav>
                </Collapse>
                </Navbar>
            </div>
        )
    }
}
export default Header