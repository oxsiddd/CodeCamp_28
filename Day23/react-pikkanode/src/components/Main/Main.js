import React, { Component } from 'react'
import { Switch, Route } from 'react-router-dom'
import Home from '../../pages/Home/Home'
import SignUp from '../../pages/SignUp/SignUp'
import SignIn from '../../pages/SignIn/SignIn'
import NoMatch from '../../pages/NoMatch/NoMatch'

class Main extends Component {
    render () {
        return (
            <Switch>
                <Route exact path='/' component={Home} />
                <Route path='/signup' component={SignUp} />
                <Route path='/signin' component={SignIn} />
                <Route component={NoMatch} />
            </Switch>
        )
    }
}

export default Main