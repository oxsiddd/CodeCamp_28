import React, { Component } from 'react'
import { Button, Form, FormGroup, Input } from 'reactstrap'

class SignUp extends Component {
    render () {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-5 mx-auto">
                        <div className="jumbotron shadow p-5 mb-5 bg-white rounded mt-5 mb-5">
                            <h3 className="mb-4">Sign Up</h3>
                            <Form onSubmit={this.handlerSubmit}>
                                <FormGroup>
                                    <Input name="email" placeholder="e-mail" />
                                    <br />
                                    <Input name="password" type="password" placeholder="password" />
                                    <br />
                                    <Input name="rePassword" type="password" placeholder="re-password" />
                                </FormGroup>
                                <Button className="btn btn-outline-dark mt-3">sign up</Button>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

handlerSubmit = e => {
    e.preventDefault()
}

const myAxios = axios.create({
    baseURL: 'http://35.240.130.197/api/v1/',
    withCredentials: true
})

async function createUser () {
    try {
        const response = await myAxios.post('auth/signup', {
            email: 'Fred',
            password: 'Flintstone'
        })
        console.log(response)
    } catch (err) {
        console.error(err)
    }
}

export default SignUp