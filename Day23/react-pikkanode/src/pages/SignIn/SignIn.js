import React, { Component } from 'react'
import { Button, Form, FormGroup, Input } from 'reactstrap'

class SignIn extends Component {
    render () {
        return (
            <div className="container">
                <div className="row">
                    <div className="col-12 col-sm-10 col-md-8 col-lg-6 col-xl-5 mx-auto">
                        <div className="jumbotron shadow p-5 mb-5 bg-white rounded mt-5 mb-5">
                            <h3 className="mb-4">Sign In</h3>
                            <Form>
                                <FormGroup>
                                    <Input name="email" placeholder="e-mail" />
                                    <br />
                                    <Input name="password" type="password" placeholder="password" />
                                </FormGroup>
                                <Button className="btn btn-outline-dark mt-3">sign in</Button>
                            </Form>
                        </div>
                    </div>
                </div>
            </div>
        )
    }
}

export default SignIn