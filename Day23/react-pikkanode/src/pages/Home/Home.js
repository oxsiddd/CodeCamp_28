import React, { Component } from 'react'
import { Container, Row, Col, CardColumns } from 'reactstrap'
import './Home.css'
import PictureCard from '../../components/PictureCard/PictureCard'

class Home extends Component {
    data = [
        {
            id: 1,
            imgSrc: "https://gitlab.com/oxsiddd/CodeCamp_28/raw/master/Day4/assets/03.jpg",
            createBy: "John",
            date: "12-07-2018",
            likeCount: "3",
            commentCount: "21"
        },
        {
            id: 2,
            imgSrc: "https://gitlab.com/oxsiddd/CodeCamp_28/raw/master/Day4/assets/01.jpg",
            createBy: "Jane",
            date: "01-03-2018",
            likeCount: "34",
            commentCount: "1"
        },
        {
            id: 3,
            imgSrc: "https://gitlab.com/oxsiddd/CodeCamp_28/raw/master/Day4/assets/06.jpg",
            createBy: "Jim",
            date: "12-01-2020",
            likeCount: "99",
            commentCount: "99"
        },
        {
            id: 4,
            imgSrc: "https://raw.githubusercontent.com/panotza/pikkanode/master/pikkanode.png",
            createBy: "Jill",
            date: "12-12-2040",
            likeCount: "911",
            commentCount: "1"
        },
        {
            id: 5,
            imgSrc: "https://gitlab.com/oxsiddd/CodeCamp_28/raw/master/Day4/assets/04.jpg",
            createBy: "Jame",
            date: "01-09-2008",
            likeCount: "1",
            commentCount: "11"
        },
        {
            id: 6,
            imgSrc: "https://gitlab.com/oxsiddd/CodeCamp_28/raw/master/Day4/assets/05.jpg",
            createBy: "Judy",
            date: "19-07-2022",
            likeCount: "33",
            commentCount: "21"
        }
    ]
    
    render () {
        return (
            <Container className="mb-5">
                <Row>
                {/* <CardColumns> */}
                    {this.data.map(item => 
                        <Col xs="12" sm="6" md="4" lg="3">
                        
                            <PictureCard 
                                key={item.id}
                                pic={item.imgSrc}
                                name={item.createBy}
                                date={item.date}
                                like={item.likeCount}
                                comment={item.commentCount}
                            />
                        
                        </Col>
                    )} 
                {/* </CardColumns>                    */}
                </Row>
                
            </Container>
        )
    }
}

export default Home