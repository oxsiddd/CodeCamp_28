import React, { Component } from 'react';
import './App.css';
import RandomBox from './RandomBox'

class App extends Component {
  color = ['red','blue','green','orange','black']
  
  randFont = (max) => {
    return Math.floor(Math.random() * Math.floor(max)) + 20
  }

  randColor = () => {
    return Math.floor(Math.random() * Math.floor(5))
  }


  render() {
    return (
      <div className="App">
        <RandomBox color={this.color[this.randColor()]} fontSize={this.randFont(50)} />
      </div>
    );
  }
}

export default App;
