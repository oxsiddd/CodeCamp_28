import React, { Component } from 'react'

class RandomBox extends Component {
    render () {
        return (
            <div style={{ backgroundColor: this.props.color, width: 240, height: 180, margin: 'auto' }}>
                <p>
                    <span style={{ fontSize: this.props.fontSize , color: 'white' }}>
                        Random Box
                    </span>
                </p>
            </div>
        )
    }
}

export default RandomBox