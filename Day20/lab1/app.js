const Koa = require('koa')
const render = require('koa-ejs')
const Router = require('koa-router')
const path = require('path')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/', async (ctx, next) => {
    
    await ctx.render('fbLogin')
    await next()
})
const fbLogin = async (ctx, next) => {

}

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)