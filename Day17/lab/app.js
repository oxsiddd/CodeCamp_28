const Koa = require('koa')
const Router = require('koa-router')

const app = new Koa()
const router = new Router()

app.use(ctx => {
    ctx.status = 400
    ctx.body = 'Something Error!!!!'
})

app.listen(8000)