const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const db = require('./lib/connect')

const app = new Koa()
const router = new Router()

router.get('/', async(ctx, next) => {
    const instructorSql = 'select c.id, c.name as course_name, i.name as instructor_name from courses as c right join instructors as i on i.id = c.teach_by where c.id is null order by c.id'
    const courseSql = 'select c.id, c.name as course_name, i.name as instructor_name from courses as c left join instructors as i on i.id = c.teach_by where i.id is null order by c.id'

    const [rowsInstructor, fieldsInstructor] = await db.query(instructorSql)
    const [rowsCourse, fieldsCourse] = await db.query(courseSql)
    // console.log(rows)
    const keyInstructor = await Object.keys(rowsInstructor[0])
    const keyCourse = await Object.keys(rowsCourse[0])
    console.log(keyInstructor)
    console.log(keyCourse)
    const data = {
        title: 'Homework13_1',
        name_table: 'Course',
        name_instructor: 'Instructors not teach',
        name_course: 'Courses not instructors',
        key_instructor: keyInstructor[2],
        data_instructor: rowsInstructor,
        key_course: keyCourse[1],
        data_course: rowsCourse
    }
    await ctx.render('home', data)
})

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)