const Koa = require('koa')
const Router = require('koa-router')
const db = require('./connect')

const app = new Koa()
const router = new Router()

router.get('/instructor/find_all', async (ctx, next) => {
    const [rows] = await db.query('SELECT * from instructors')
    ctx.body = JSON.stringify(rows, undefined, 2)
    await next()
})

router.get('/instructor/find_by_id/:id', async (ctx, next) => {
    const [rows] = await db.query('SELECT * from instructors WHERE id = ?',[ctx.params.id])
    ctx.body = JSON.stringify(rows, undefined, 2)
    await next()
})

router.get('/course/find_by_id/:id', async (ctx, next) => {
    const [rows] = await db.query('SELECT * from courses WHERE id = ?',[ctx.params.id])
    ctx.body = JSON.stringify(rows, undefined, 2)
    await next()
})

router.get('/course/find_by_price/:price', async (ctx, next) => {
    const [rows] = await db.query('SELECT * from courses WHERE price = ?',[ctx.params.price])
    ctx.body = JSON.stringify(rows, undefined, 2)
    await next()
})

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)