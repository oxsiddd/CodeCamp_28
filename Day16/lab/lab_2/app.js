const Koa = require('koa')
const Router = require('koa-router')
const db = require('./connect')

const app = new Koa()
const router = new Router()

router.get('/normal_query/:id', async (ctx, next) => {
    const [rows] = await db.query('SELECT * from users WHERE id = ?',[ctx.params.id]);
    ctx.body = rows;
    await next();
 }) 

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)



