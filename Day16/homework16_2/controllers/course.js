module.exports = function (courseModel, pool) {
    return {
        async findCourseById(ctx, next) {
            const courseObject = await courseModel.findById(pool, ctx.params.id)
            const keyCourse = await Object.keys(courseObject)
            const data = {
                title: 'homework16_2',
                data: [courseObject],
                key: keyCourse,
                nameTable: 'This Course id: ' + courseObject.id
            }
            await ctx.render('table_course', data)
            await next()
        },
        async  findCourseByPrice(ctx, next) {
            const courseObject = await courseModel.findByPrice(pool, ctx.params.price)
            const keyCourse = await Object.keys(courseObject[0])
            // console.log(courseObject)
            // console.log(keyCourse)
            const data = {
                title: 'homework16_2',
                data: courseObject,
                key: keyCourse,
                nameTable: 'This Course price: ' + courseObject[0].price
            }
            await ctx.render('table_course', data)
            await next()
        }
    }
}