module.exports = function (instructorModel, pool) {
    return {
        async findInstructorById(ctx, next) {
            const instructorObject = await instructorModel.findById(pool, ctx.params.id)
            const keyInstructor = await Object.keys(instructorObject)
            // console.log(instructorObject)
            // console.log(keyInstructor)
            const data = {
                title: 'homework16_2',
                data: [instructorObject],
                key: keyInstructor,
                nameTable: 'This Instructor id: ' + instructorObject.id
            }
            await ctx.render('table_instructor', data)
            await next()
        },
        async  findInstructorAll(ctx, next) {
            const instructorObject = await instructorModel.findAll(pool)
            const keyInstructor = await Object.keys(instructorObject[0])
            // console.log(instructorObject)
            // console.log(keyInstructor)
            const data = {
                title: 'homework16_2',
                data: instructorObject,
                key: keyInstructor,
                nameTable: 'All Instructors'
            }
            await ctx.render('table_instructor', data)
            await next()
        }
    }
}