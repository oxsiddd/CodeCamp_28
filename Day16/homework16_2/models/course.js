module.exports = {
    async createEntity(row) {
        if (!row.id)
            return {}

        return {
            id: row.id,
            name: row.name,
            detail: row.detail,
            price: row.price,
            teach_by: row.teach_by,
            created_at: row.created_at
        }
    },
    async findById(pool, id) {
        const [rows] = await pool.query('SELECT * FROM courses WHERE id = ?', [id])
        return this.createEntity(rows[0])
    },
    async findByPrice(pool, price) {
        const [rows] = await pool.query('SELECT * FROM courses WHERE price = ?', [price])
        return rows
    },
}