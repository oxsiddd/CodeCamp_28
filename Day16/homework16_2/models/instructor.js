module.exports = {
    async createEntity(row) {
        if (!row.id)
            return {}

        return {
            id: row.id,
            name: row.name,
            created_at: row.created_at
        }
    },
    async findAll(pool) {
        const [rows] = await pool.query('SELECT * FROM instructors')
        //console.log(rows)
        return rows
    },
    async findById(pool, id) {
        const [rows] = await pool.query('SELECT * FROM instructors WHERE id = ?', [id])
        return this.createEntity(rows[0])
    }
}