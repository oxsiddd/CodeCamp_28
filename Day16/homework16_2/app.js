const Koa = require('koa')
const Router = require('koa-router')
const render = require('koa-ejs')
const path = require('path')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

const pool = require('./libs/connect')

const instructorModel = require('./models/instructor')
const courseModel = require('./models/course')
const instructorTemp = require('./controllers/instructor')
const instructorController = instructorTemp(instructorModel, pool)
const courseTemp = require('./controllers/course')
const courseController = courseTemp(courseModel, pool)

router.get('/instructor/find_all', instructorController.findInstructorAll)

router.get('/instructor/find_by_id/:id', instructorController.findInstructorById)

router.get('/course/find_by_id/:id', courseController.findCourseById)

router.get('/course/find_by_price/:price', courseController.findCourseByPrice)

app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)