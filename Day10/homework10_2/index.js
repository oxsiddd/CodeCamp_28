const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')

const app = new Koa()
const router = new Router()

router.get('/', async(ctx, next) => {
    let data = {
        title: 'About Me'
    }
    await ctx.render('home', data)
})

router.get('/skill', async(ctx, next) => {
    let data = {
        title: 'My Skill'
    }
    await ctx.render('skill', data)
})

router.get('/contact', async(ctx, next) => {
    let data = {
        title: 'Contact Me'
    }
    await ctx.render('contact', data)
})

router.get('/portfolio', async(ctx, next) => {
    let data = {
        title: 'Portfolio'
    }
    await ctx.render('portfolio', data)
})


render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)