const Koa = require('koa')
const Router = require('koa-router')
const serve = require('koa-static')
const path = require('path')
const render = require('koa-ejs')
const fs = require('fs')

const app = new Koa()
const router = new Router()

render(app, {
    root: path.join(__dirname, 'views'),
    layout: 'template',
    viewExt: 'ejs',
    cache: false
})

router.get('/', async(ctx, next) => {
    const resultJSON = await new Promise(function(resolve, reject){
        fs.readFile('../../28Apr/homework2_1.json', function(err, data){
            if(err)
                reject(err)
            else
                resolve(data)
        })
    })
    //console.log(resultJSON)
    const dataResult = JSON.parse(resultJSON)
    //console.log(changeResult)
    const resultKey = Object.keys(dataResult[0])
    //console.log(resultKey)

    const data = {
        title: 'Homework10_3',
        header: 'Homework10_3',
        key: resultKey,
        data: dataResult
    }
    //console.log(data.data)
    await ctx.render('landing', data)
})




app.use(serve(path.join(__dirname, 'public')))
app.use(router.routes())
app.use(router.allowedMethods())
app.listen(3000)